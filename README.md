# Screen Worlds
A procedural animated planet landscape wallpaper for X11

![Screenshot](screenworlds.png)

Inspired by Daniel Linssen's [Planetarium](https://managore.itch.io/planetarium)

## Requirements
Screen Worlds requires SDL2 to build and run, and xwinwrap to function as a wallpaper.

## Build
```shell
cmake .
make
```
The executable file is located in the `build/` directory. Place it anywhere you want, with the `config.txt` alongside it.

## Usage

### Standalone
Screen Worlds can be used as a stand-alone program if no commandline parameters are supplied: 
`./screenworlds`

Hit R to reload the options and generate a new world. Hit Escape to quit.

### Wallpaper
Using the program as a wallpaper for X11 requires [xwinwrap](https://github.com/ujjwal96/xwinwrap):

`xwinwrap [OPTIONS] -- ./screenworlds WID`

The options supplied to xwinwrap will vary depending on your desktop environment. The "WID" string must be included as-is and will be filled by xwinwrap.

To reload the options and generate a new world, send a SIGUSR1 signal to the process:

`kill -USR1 $(pidof screenworlds)`

## Options

The config.txt file contains all the rendering and style options. Each option is specified on its own line as a key-value pair, separated by a space character. Values are in hexadecimal form only, colors in RRGGBB.

Option | Description
------ | -----------
render-scale | Zoom factor
frame-rate | Target frame rate
radius-max | Maximum radius for satellite and ring orbits
orbit-speed | Global speed of satellites and rings
orbit-speed-dev | Maximum deviation of the orbit speed
shadow | Enable sun shadows
shadow-rotation | Enable sun shadow rotation
shadow-speed | Sun shadow rotation speed in seconds per revolution (higher is slower)
shadow-speed-dev | Maximum deviation of the shadow speed
planet-rotation | Enable planet rotation
planet-speed | Planet rotation speed in seconds per revolution
planet-speed-dev | Maximum deviation of the planet speed
planet-min-size | Minimum planet radius
planet-max-size | Maximum planet radius
planet-color-steps | Number of steps for the planet gradient in each color group
planet-color-count | Number of planet color groups
satellite-min-count | Minimum number of satellites
satellite-max-count | Maximum number of satellites
satellite-min-size | Minimum satellite radius
satellite-max-size | Maximum satellite radius
satellite-color-count | Number of satellite colors
ring-min-count | Minimum number of rings
ring-max-count | Minimum number of rings
ring-density | Density of the ring particles
ring-density-dev | Maximum deviation of the ring density
ring-width | Width of the ring
ring-width-dev | Maximum deviation of the ring width
ring-color-steps | Number of different colors in each ring color group
ring-color-count | Number of ring color groups
star-density | Density of the background stars
star-shadow | Enable shading of the stars behind the planet
star-milkyway | Proportion of milky way-distributed stars (0 to disable, higher is more present)
star-range | Range of the milky way pattern (lower is largeer, higher is shorter)
star-spread | Spread of the milky way pattern (lower is more spread, higher is denser)
star-twinkling | Enable star twinkling
star-twinkle-period | Star twinkling frequency (lower is more frequent)
color-sky | Background sky color
color-planet-X-Y | Planet color (Xth group, Yth step)
color-planet-shadow-X-Y | Planet shadow color (Xth group, Yth step)
color-satellite-X | Satellite color (Xth color)
color-satellite-shadow-X | Satellite shadow color (Xth color)
color-ring-X-Y | Ring color (Xth group, Yth color)
color-star | Background star color
color-star-twinkle | Star twinkle color
