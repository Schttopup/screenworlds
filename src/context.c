#include "context.h"
#include "defs.h"
#include "worlds.h"
#include <stdio.h>
#include <stdbool.h>


char const * const optionNames[OPTION_LAST] = {
        "",
        "render-scale",
        "frame-rate",
        "radius-max",
        "orbit-speed",
        "orbit-speed-dev",
        "shadow",
        "shadow-rotation",
        "shadow-speed",
        "shadow-speed-dev",
        "planet-min-size",
        "planet-max-size",
        "planet-rotation",
        "planet-speed",
        "planet-speed-dev",
        "planet-color-steps",
        "planet-color-count",
        "satellite-min-count",
        "satellite-max-count",
        "satellite-min-size",
        "satellite-max-size",
        "satellite-color-count",
        "ring-min-count",
        "ring-max-count",
        "ring-density",
        "ring-density-dev",
        "ring-width",
        "ring-width-dev",
        "ring-color-steps",
        "ring-color-count",
        "star-density",
        "star-shadow",
        "star-milkyway",
        "star-range",
        "star-spread",
        "star-twinkling",
        "star-twinkle-period"
};


#if defined(__unix__) || defined(__linux__)
#include<unistd.h>
#elif defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif

char* configFileName(void)
{
    static char fullPath[MAX_PATH];
#if defined(__unix__) || defined(__linux__)
#elif defined(_WIN32) || defined(_WIN64)
    size_t len = sizeof(fullPath);
    GetModuleFileNameA(NULL, fullPath, len);
    char *ns = strrchr(fullPath, '/');
    char *nb = strrchr(fullPath, '\\');
    char *end = fullPath;
    if(ns)
        end = ns;
    if(nb && nb > ns)
        end = nb;
    strcpy(end + 1, "config.txt");
#endif
    return fullPath;
}


Context *makeContext(SDL_Renderer *renderer)
{
    if(!renderer)
        return NULL;
    Context *context = NULL;
    MMALLOC(context, Context, 1);
    context->options = NULL;
    context->colorids = NULL;
    context->colors = NULL;
    readOptions(context);

    context->renderer = renderer;
    int windowWidth, windowHeight;
    int scale = getOption(context, OPTION_RENDER_SCALE);
    SDL_GetRendererOutputSize(renderer, &windowWidth, &windowHeight);
    context->width = windowWidth / scale;
    context->height = windowHeight / scale;
    context->renderTexture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_STREAMING, context->width, context->height);

    context->perlinSeed = NULL;
    perlinInit(context);
    MMALLOC(context->renderZ, int, context->width * context->height);

    return context;
}

void destroyContext(Context *context)
{
    if(!context)
        return;
    FFREE(context->options);
    FFREE(context->colorids);
    FFREE(context->colors);
    FFREE(context->perlinSeed);
    FFREE(context->renderZ);
    SDL_DestroyTexture(context->renderTexture);
    SDL_DestroyRenderer(context->renderer);
}

void resizeContext(Context *context)
{
    if(!context)
        return;
    int windowWidth, windowHeight;
    int scale = getOption(context, OPTION_RENDER_SCALE);
    SDL_GetRendererOutputSize(context->renderer, &windowWidth, &windowHeight);
    SDL_DestroyTexture(context->renderTexture);
    context->width = windowWidth / scale;
    if(windowWidth % scale) context->width++;
    context->height = windowHeight / scale;
    if(windowHeight % scale) context->height++;
    context->renderTexture = SDL_CreateTexture(context->renderer, SDL_PIXELFORMAT_RGB888, SDL_TEXTUREACCESS_STREAMING, context->width, context->height);
    RREALLOC(context->renderZ, int, context->width * context->height);
}


void readOptions(Context *context)
{
    if(!context)
        return;

    FILE *config = fopen(configFileName(), "r");
    if(!config)
        return;
    int fileSize = 0;
    const int chunkSize = 4096;
    char *text = NULL;
    while(true)
    {
        RREALLOC(text, char, fileSize + chunkSize);
        int bytes = fread(text + fileSize, 1, chunkSize, config);
        if(bytes <= 0)
            break;
        fileSize += bytes;
    }
    RREALLOC(text, char, fileSize + 1);
    text[fileSize] = '\0';
    fclose(config);

    char **keys = NULL;
    int *values = NULL;
    int keyCount = 0;
    bool isKey = true;
    int keyStart = 0, keyEnd = 0, valStart = 0;
    for(int index = 0; text[index]; index++)
    {
        char c = text[index];
        if(c == '\n' || c == '\r')
        {
            if(!isKey)
            {
                char *key = NULL;
                MMALLOC(key, char, keyEnd - keyStart + 1);
                for(int i = 0; i < keyEnd - keyStart; i++)
                    key[i] = text[keyStart + i];
                key[keyEnd - keyStart] = '\0';
                int value = getNumber(text + valStart, keyEnd - keyStart);
                RREALLOC(keys, char*, keyCount + 1);
                keys[keyCount] = key;
                RREALLOC(values, int, keyCount + 1);
                values[keyCount] = value;
                keyCount++;
                isKey = true;
            }
            keyStart = index + 1;
            continue;
        }
        if(c == ' ' || c == '\t')
        {
            if(isKey && index > keyStart)
            {
                keyEnd = index;
            }
            isKey = false;
            valStart = index + 1;
            continue;
        }
    }

    FFREE(text);

    if(context->options == NULL)
        MMALLOC(context->options, int, OPTION_LAST);
    for(int i = 0; i < OPTION_LAST; i++)
        context->options[i] = 0;
    if(context->colorids == NULL)
        MMALLOC(context->colorids, int, COLOR_LAST);
    for(int i = 0; i < COLOR_LAST; i++)
        context->colorids[i] = 0;

    for(int i = 0; i < keyCount; i++)
    {
        for(int j = 1; j < OPTION_LAST; j++)
        {
            if(strcmp(keys[i], optionNames[j]) == 0)
            {
                context->options[j] = values[i];
                break;
            }
        }
    }

    int colorCount = 0;
    context->colorids[COLOR_SKY] = colorCount;
    colorCount++;
    context->colorids[COLOR_PLANET] = colorCount;
    int colorCountInc = getOption(context, OPTION_PLANET_COLOR_COUNT) * getOption(context, OPTION_PLANET_COLOR_STEPS);
    colorCount += colorCountInc;
    context->colorids[COLOR_PLANET_SHADOW] = colorCount;
    colorCount += colorCountInc;
    context->colorids[COLOR_SATELLITE] = colorCount;
    colorCountInc = getOption(context, OPTION_SATELLITE_COLOR_COUNT);
    colorCount += colorCountInc;
    context->colorids[COLOR_SATELLITE_SHADOW] = colorCount;
    colorCount += colorCountInc;
    context->colorids[COLOR_RING] = colorCount;
    colorCountInc = getOption(context, OPTION_RING_COLOR_COUNT) * getOption(context, OPTION_RING_COLOR_STEPS);
    colorCount += colorCountInc;
    context->colorids[COLOR_STAR] = colorCount;
    colorCount++;
    context->colorids[COLOR_STAR_TWINKLE] = colorCount;
    colorCount++;

    char colorBuf[100] = {'\0'};
    if(context->colors != NULL)
        FFREE(context->colors);
    MMALLOC(context->colors, Uint32, colorCount);
    for(int i = 0; i < colorCount; i++)
        context->colors[i] = 0;

    context->colors[context->colorids[COLOR_SKY]] = values[getOptionString(keys, keyCount, "color-sky")];
    int cSteps = getOption(context, OPTION_PLANET_COLOR_STEPS);
    int cCount = getOption(context, OPTION_PLANET_COLOR_COUNT);
    for(int i = 0; i < cCount; i++)
    {
        for(int j = 0; j < cSteps; j++)
        {
            snprintf(colorBuf, 100, "color-planet-%d-%d", i + 1, j + 1);
            context->colors[context->colorids[COLOR_PLANET] + (i * cSteps) + j] = values[getOptionString(keys, keyCount, colorBuf)];
            snprintf(colorBuf, 100, "color-planet-shadow-%d-%d", i + 1, j + 1);
            context->colors[context->colorids[COLOR_PLANET_SHADOW] + (i * cSteps) + j] = values[getOptionString(keys, keyCount, colorBuf)];
        }
    }
    cCount = getOption(context, OPTION_SATELLITE_COLOR_COUNT);
    for(int i = 0; i < cCount; i++)
    {
        snprintf(colorBuf, 100, "color-satellite-%d", i + 1);
        context->colors[context->colorids[COLOR_SATELLITE] + i] = values[getOptionString(keys, keyCount, colorBuf)];
        snprintf(colorBuf, 100, "color-satellite-shadow-%d", i + 1);
        context->colors[context->colorids[COLOR_SATELLITE_SHADOW] + i] = values[getOptionString(keys, keyCount, colorBuf)];
    }
    cSteps = getOption(context, OPTION_RING_COLOR_STEPS);
    cCount = getOption(context, OPTION_RING_COLOR_COUNT);
    for(int i = 0; i < cCount; i++)
    {
        for(int j = 0; j < cSteps; j++)
        {
            snprintf(colorBuf, 100, "color-ring-%d-%d", i + 1, j + 1);
            context->colors[context->colorids[COLOR_RING] + (i * cSteps) + j] = values[getOptionString(keys, keyCount, colorBuf)];
        }
    }
    context->colors[context->colorids[COLOR_STAR]] = values[getOptionString(keys, keyCount, "color-star")];
    context->colors[context->colorids[COLOR_STAR_TWINKLE]] = values[getOptionString(keys, keyCount, "color-star-twinkle")];

    for(int i = 0; i < keyCount; i++)
        FFREE(keys[i]);
    FFREE(keys);
    FFREE(values);
}

int getNumber(char *start, int length)
{
    if(!start)
        return 0;
    int number = 0;
    for(int i = 0; i < length && i < 6; i++)
    {
        char c = start[i];
        if('0' <= c && c <= '9')
            number = (number << 4) + (c - '0');
        else if('A' <= c && c <= 'F')
            number = (number << 4) + (c - 'A' + 10);
        else if('a' <= c && c <= 'f')
            number = (number << 4) + (c - 'a' + 10);
        else
            return number;
    }
    return number;
}

int getOptionString(char **keys, int keyCount, char *string)
{
    for(int i = 1; i < keyCount; i++)
    {
        if(strcmp(string, keys[i]) == 0)
        {
            return i;
        }
    }
    return 0;
}

int getOption(Context *context, enum Options_ option)
{
    if(!context)
        return 0;
    if(option > OPTION_NONE && option < OPTION_LAST)
        return context->options[option];
    return 0;
}

