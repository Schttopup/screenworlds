#include <SDL2/SDL.h>
#include "context.h"


struct Stars_
{
    int count;
    int *x;
    int *y;
    int *sizes;
    int *twinkling;
    float twinkleTime;
};
typedef struct Stars_ Stars;

struct Trajectory_
{
    float pitch;
    float yaw;
    float x;
    float y;
    float z;
    float radius;
    float speed;
};
typedef struct Trajectory_ Trajectory;

struct Ring_
{
    Trajectory orbit;
    float width;
    int count;
    int *sizes;
    int *distances;
    float *positions;
    int *colors;
    int colorNo;
};
typedef struct Ring_ Ring;

struct Satellite_
{
    Trajectory orbit;
    int size;
    float position;
    int colorNo;
};
typedef struct Satellite_ Satellite;

struct Planet_
{
    Trajectory axis;
    int size;
    float rotation;
    float perlinScale;
    int colorNo;
};
typedef struct Planet_ Planet;

struct World_
{
    Stars *stars;
    Planet *planet;
    Satellite **satellites;
    int satelliteCount;
    Ring **rings;
    int ringCount;
    Trajectory sun;
    float sunPosition;
};
typedef struct World_ World;


Planet* makePlanet(Context *context);
Satellite* makeSatellite(Context *context, float radius);
Ring* makeRing(Context *context, float radius);
Stars* makeStars(Context *context);
World* makeWorld(Context *context);

void buildTrajectory(Trajectory *trajectory);

void destroyPlanet(Planet *planet);
void destroySatellite(Satellite *satellite);
void destroyRing(Ring *ring);
void destroyStars(Stars *stars);
void destroyWorld(World *world);

void drawPlanet(Context *context, Planet *planet, World *world);
void drawSatellite(Context *context, Satellite *satellite, World *world);
void drawRing(Context *context, Ring *ring);
void drawStars(Context *context, Stars *stars);
void drawWorld(Context *context, World *world);

Uint32 interpolate(Uint32 color1, Uint32 color2, float alpha);
void setPixel(Context *context, int x, int y, int z, int c);

void tickWorld(Context *context, World *world, float timeDiff);

void getCoords(Trajectory *trajectory, float position, float *x, float *y, float *z);
int getSphereCoords(float x, float y, float *z);
void rotateCoords(Trajectory *axis, float *x, float *y, float *z, float angle);

void perlinInit(Context *context);
float perlinFade(float t);
int perlinInc(int num);
float perlinGrad(int hash, float x, float y, float z);
float perlinLerp(float a, float b, float x);
float perlin(Context *context, float x, float y, float z);
float perlinOctave(Context *context, float x, float y, float z, int octaves, float persistence);

