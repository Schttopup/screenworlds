#ifndef SDEFS_H
#define SDEFS_H

#include <stdlib.h>

#define MMIN(X, Y) (((X) <= (Y)) ? (X) : (Y))
#define MMAX(X, Y) (((X) >= (Y)) ? (X) : (Y))

#define UNUSED(x) (void)(x)

#define MALLOC(t, n) (t*) malloc((n) * sizeof(t))
#define MCHECK(x) if(!(x)){exit(EXIT_FAILURE);}
#define MMALLOC(p, t, n) {if((n) == 0){free(p);(p)=NULL;}else{(p) = MALLOC(t, n); MCHECK(p)}}

#define REALLOC(t, p, n) (t*) realloc(p, (n) * sizeof(t))
#define RCHECK(x) if(!(x)){exit(EXIT_FAILURE);}
#define RREALLOC(p, t, n) {if((n) == 0){free(p);(p)=NULL;}else{(p) = REALLOC(t, p, n); RCHECK(p)}}

#define FFREE(p) {free(p);(p)=NULL;}

#endif // SDEFS_H
