#ifndef SCREENWORLDS_CONTEXT_H
#define SCREENWORLDS_CONTEXT_H

#include <SDL2/SDL.h>


struct Context_
{
    SDL_Renderer *renderer;
    SDL_Texture *renderTexture;
    Uint32 *pixels;
    int width;
    int height;
    int *renderZ;
    int *perlinSeed;
    int *options;
    int *colorids;
    Uint32 *colors;
};
typedef struct Context_ Context;


enum Options_
{
    OPTION_NONE,
    OPTION_RENDER_SCALE,
    OPTION_FRAME_RATE,
    OPTION_RADIUS_MAX,
    OPTION_ORBIT_SPEED,
    OPTION_ORBIT_SPEED_DEV,
    OPTION_SHADOW,
    OPTION_SHADOW_ROTATION,
    OPTION_SHADOW_SPEED,
    OPTION_SHADOW_SPEED_DEV,
    OPTION_PLANET_MIN_SIZE,
    OPTION_PLANET_MAX_SIZE,
    OPTION_PLANET_ROTATION,
    OPTION_PLANET_SPEED,
    OPTION_PLANET_SPEED_DEV,
    OPTION_PLANET_COLOR_STEPS,
    OPTION_PLANET_COLOR_COUNT,
    OPTION_SATELLITE_MIN_COUNT,
    OPTION_SATELLITE_MAX_COUNT,
    OPTION_SATELLITE_MIN_SIZE,
    OPTION_SATELLITE_MAX_SIZE,
    OPTION_SATELLITE_COLOR_COUNT,
    OPTION_RING_MIN_COUNT,
    OPTION_RING_MAX_COUNT,
    OPTION_RING_DENSITY,
    OPTION_RING_DENSITY_DEV,
    OPTION_RING_WIDTH,
    OPTION_RING_WIDTH_DEV,
    OPTION_RING_COLOR_STEPS,
    OPTION_RING_COLOR_COUNT,
    OPTION_STAR_DENSITY,
    OPTION_STAR_SHADOW,
    OPTION_STAR_MILKYWAY,
    OPTION_STAR_RANGE,
    OPTION_STAR_SPREAD,
    OPTION_STAR_TWINKLING,
    OPTION_STAR_TWINKLE_PERIOD,
    OPTION_LAST
};

extern char const * const optionNames[OPTION_LAST];

enum ColorIds_
{
    COLOR_NONE,
    COLOR_SKY,
    COLOR_PLANET,
    COLOR_PLANET_SHADOW,
    COLOR_SATELLITE,
    COLOR_SATELLITE_SHADOW,
    COLOR_RING,
    COLOR_STAR,
    COLOR_STAR_TWINKLE,
    COLOR_LAST
};

char* configFileName(void);

Context *makeContext(SDL_Renderer *renderer);
void destroyContext(Context *context);
void resizeContext(Context *context);

void readOptions(Context *context);
int getNumber(char *start, int length);
int getOptionString(char **keys, int keyCount, char *string);
int getOption(Context *context, enum Options_ option);


#endif //SCREENWORLDS_CONTEXT_H
