#include "worlds.h"
#include <SDL2/SDL.h>
#include "defs.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdbool.h>


#define RAND() ((float)rand() / (float)RAND_MAX)
#define MAX_TRIES 10
#define BASE_RADIUS 100
#define BASE_WIDTH 10
#define BASE_RESOLUTION (100 * 100)
#define RINGPART_SIZE_MAX 2
#define STAR_SIZE_MAX 3
#define STAR_TWINKLE_TIME 0.05
#define STAR_TWINKLE_FRAMES 4


Planet* makePlanet(Context *context)
{
    if(!context)
        return NULL;
    Planet *planet = NULL;
    MMALLOC(planet, Planet, 1);
    int minSize = getOption(context, OPTION_PLANET_MIN_SIZE);
    int maxSize = getOption(context, OPTION_PLANET_MAX_SIZE);
    if(maxSize <= minSize) maxSize = minSize + 1;
    float speed = 1.0 / (float)getOption(context, OPTION_PLANET_SPEED);
    float speedDev = (float)getOption(context, OPTION_PLANET_SPEED_DEV) / (float)getOption(context, OPTION_PLANET_SPEED);
    planet->size = (rand() % (maxSize - minSize)) + minSize;
    planet->axis.pitch = RAND() * 0.5 - 0.25;
    planet->axis.yaw = RAND();
    planet->axis.speed = ((RAND() * 2.0 - 1.0) * speedDev * speed) + speed;
    buildTrajectory(&planet->axis);
    if(RAND() > 0.5) planet->axis.speed = -planet->axis.speed;
    planet->rotation = RAND();
    planet->perlinScale = (RAND() + 0.5) * 300.0;
    planet->colorNo = rand() % getOption(context, OPTION_PLANET_COLOR_COUNT);
    return planet;
}

Satellite* makeSatellite(Context *context, float radius)
{
    if(!context)
        return NULL;
    Satellite *satellite = NULL;
    MMALLOC(satellite, Satellite, 1);
    int minSize = getOption(context, OPTION_SATELLITE_MIN_SIZE);
    int maxSize = getOption(context, OPTION_SATELLITE_MAX_SIZE);
    float speed = 1.0 / (float)getOption(context, OPTION_ORBIT_SPEED);
    float speedDev = (float)getOption(context, OPTION_ORBIT_SPEED_DEV) / (float)getOption(context, OPTION_ORBIT_SPEED);
    satellite->size = (rand() % (maxSize - minSize)) + minSize;
    satellite->orbit.pitch = RAND() * 0.5 - 0.25;
    satellite->orbit.yaw = RAND();
    satellite->orbit.radius = radius;
    satellite->orbit.speed = (((RAND() * 2.0 - 1.0) * speedDev * speed) + speed) * (BASE_RADIUS / (sqrtf(satellite->orbit.radius * satellite->orbit.radius)));
    buildTrajectory(&satellite->orbit);
    if(RAND() > 0.5) satellite->orbit.speed = -satellite->orbit.speed;
    satellite->position = RAND();
    satellite->colorNo = rand() % getOption(context, OPTION_SATELLITE_COLOR_COUNT);
    return satellite;
}

Ring* makeRing(Context *context, float radius)
{
    if(!context)
        return NULL;
    Ring *ring = NULL;
    MMALLOC(ring, Ring, 1);
    float speed = 1.0 / (float)getOption(context, OPTION_ORBIT_SPEED);
    float speedDev = (float)getOption(context, OPTION_ORBIT_SPEED_DEV) / (float)getOption(context, OPTION_ORBIT_SPEED);
    float width = (float)getOption(context, OPTION_RING_WIDTH) / 100.0;
    float widthDev = (float)getOption(context, OPTION_RING_WIDTH_DEV) / 100.0;
    float density = (float)getOption(context, OPTION_RING_DENSITY);
    float densityDev = (float)getOption(context, OPTION_RING_DENSITY_DEV);
    ring->orbit.pitch = RAND() * 0.5 - 0.25;
    ring->orbit.yaw = RAND();
    ring->orbit.radius = radius;
    ring->orbit.speed = (((RAND() * 2.0 - 1.0) * speedDev * speed) + speed) * (BASE_RADIUS / (sqrtf(ring->orbit.radius * ring->orbit.radius)));
    buildTrajectory(&ring->orbit);
    if(RAND() > 0.5) ring->orbit.speed = -ring->orbit.speed;
    ring->width = ((RAND() * 2.0 - 1.0) * widthDev + width) * radius;
    ring->count = ((RAND() * 2.0 - 1.0) * densityDev + density) * (ring->width / BASE_WIDTH) * ((radius) / (BASE_RADIUS));
    ring->sizes = NULL;
    MMALLOC(ring->sizes, int, ring->count);
    ring->distances = NULL;
    MMALLOC(ring->distances, int, ring->count);
    ring->positions = NULL;
    MMALLOC(ring->positions, float, ring->count);
    ring->colors = NULL;
    MMALLOC(ring->colors, int, ring->count);
    ring->colorNo = rand() % getOption(context, OPTION_RING_COLOR_COUNT);
    int colorSteps = getOption(context, OPTION_RING_COLOR_STEPS);
    for(int i = 0; i < ring->count; i++)
    {
        ring->distances[i] = (rand() % (int) (ring->width)) - (ring->width / 2) + ring->orbit.radius;
        ring->sizes[i] = rand() % RINGPART_SIZE_MAX + 1;
        ring->positions[i] = RAND();
        int colorStep = rand() % colorSteps;
        ring->colors[i] = colorStep;
    }
    return ring;
}

Stars* makeStars(Context *context)
{
    if(!context)
        return NULL;
    Stars *stars = NULL;
    MMALLOC(stars, Stars, 1);
    int surface = context->width * context->height;
    int milkyway = getOption(context, OPTION_STAR_MILKYWAY);
    float maxWindowRadius = sqrtf(context->width * context->width + context->height * context->height);
    float maxRadius = getOption(context, OPTION_RADIUS_MAX);
    float scaledRadius = (maxRadius + maxWindowRadius) / getOption(context, OPTION_STAR_RANGE);
    float spread = getOption(context, OPTION_STAR_SPREAD);
    float px = RAND() - 0.5;
    float px2 = RAND() - 0.5;
    float px3 = RAND() - 0.5;
    float angle = RAND() * M_PI * 2.0;

    stars->count = getOption(context, OPTION_STAR_DENSITY) * ((scaledRadius * scaledRadius) / (float)(BASE_RADIUS * BASE_RADIUS));
    stars->x = NULL;
    MMALLOC(stars->x, int, stars->count);
    stars->y = NULL;
    MMALLOC(stars->y, int, stars->count);
    stars->sizes = NULL;
    MMALLOC(stars->sizes, int, stars->count);
    stars->twinkling = NULL;
    MMALLOC(stars->twinkling, int, stars->count);
    for(int i = 0; i < stars->count; i++)
    {
        if(milkyway && i % (milkyway) != 0)
        {
            float x = RAND() * 2.0 - 1.0;
            //float y = (x * (x * (x * px3 + px2) + px));
            float y = px3 * x * x * x + px2 * x * x + px * x;
            float randAngle = RAND() * M_PI * 2.0;
            float randDistance = RAND() * (1.0 / spread);
            float rx = x * cosf(angle) - y * sinf(angle) + (randDistance * cosf(randAngle));
            float ry = x * sinf(angle) + y * cosf(angle) + (randDistance * sinf(randAngle));
            stars->x[i] = (rx * scaledRadius);
            stars->y[i] = (ry * scaledRadius);
        }
        else
        {
            stars->x[i] = (rand() % context->width) - (context->width / 2);
            stars->y[i] = (rand() % context->height) - (context->height / 2);
        }
        stars->sizes[i] = (rand() % STAR_SIZE_MAX);
        stars->twinkling[i] = 0;
    }
    stars->twinkleTime = 0;
    return stars;
}


World* makeWorld(Context *context)
{
    World *world = NULL;
    MMALLOC(world, World, 1);
    world->stars = makeStars(context);
    world->planet = makePlanet(context);
    int minRadius = world->planet->size * 2;
    int maxRadius = getOption(context, OPTION_RADIUS_MAX);

    int maxSatellites = getOption(context, OPTION_SATELLITE_MAX_COUNT);
    world->satelliteCount = getOption(context, OPTION_SATELLITE_MIN_COUNT);
    for(int i = world->satelliteCount; rand() % (maxSatellites + 1) < maxSatellites - i; i++)
        world->satelliteCount++;
    world->satellites = NULL;
    MMALLOC(world->satellites, Satellite *, world->satelliteCount);
    for(int i = 0; i < world->satelliteCount; i++)
    {
        int satelliteRadius = rand() % (maxRadius - minRadius) + minRadius;
        world->satellites[i] = makeSatellite(context, satelliteRadius);
    }

    int maxRings = getOption(context, OPTION_RING_MAX_COUNT);
    world->ringCount = getOption(context, OPTION_RING_MIN_COUNT);
    for(int i = world->ringCount; rand() % (maxRings + 1) < maxRings - i; i++)
        world->ringCount++;
    world->rings = NULL;
    MMALLOC(world->rings, Ring *, world->ringCount);
    for(int i = 0; i < world->ringCount; i++)
    {
        int ringRadius = rand() % (maxRadius - minRadius) + minRadius;
        world->rings[i] = makeRing(context, ringRadius);
    }

    world->sun.pitch = RAND() * 0.5 - 0.25;
    world->sun.yaw = RAND();
    world->sun.radius = 1;
    float speed = 1.0 / (float)getOption(context, OPTION_SHADOW_SPEED);
    float speedDev = (float)getOption(context, OPTION_SHADOW_SPEED_DEV) / (float)getOption(context, OPTION_SHADOW_SPEED);
    world->sun.speed = (((RAND() * 2.0 - 1.0) * speedDev * speed) + speed);
    world->sunPosition = RAND();
    buildTrajectory(&world->sun);

    return world;
}


void buildTrajectory(Trajectory *trajectory)
{
    if(!trajectory)
        return;
    float yaw = trajectory->yaw * M_PI * 2;
    float pitch = trajectory->pitch * M_PI * 2;
    trajectory->x = cosf(pitch) * cosf(yaw);
    trajectory->y = -sinf(pitch);
    trajectory->z = cosf(pitch) * sinf(yaw);
    float norm = sqrtf(trajectory->x * trajectory->x + trajectory->y * trajectory->y + trajectory->z * trajectory->z);
    trajectory->x /= norm;
    trajectory->y /= norm;
    trajectory->z /= norm;
}


void destroyPlanet(Planet *planet)
{
    if(!planet)
        return;
    FFREE(planet);
}

void destroySatellite(Satellite *satellite)
{
    if(!satellite)
        return;
    FFREE(satellite);
}

void destroyRing(Ring *ring)
{
    if(!ring)
        return;
    FFREE(ring->sizes);
    FFREE(ring->distances);
    FFREE(ring->positions);
    FFREE(ring);
}

void destroyStars(Stars *stars)
{
    if(!stars)
        return;
    FFREE(stars->x);
    FFREE(stars->y);
    FFREE(stars->sizes);
    FFREE(stars);
}

void destroyWorld(World *world)
{
    if(!world)
        return;
    destroyStars(world->stars);
    destroyPlanet(world->planet);
    for(int i = 0; i < world->satelliteCount; i++)
        destroySatellite(world->satellites[i]);
    FFREE(world->satellites);
    for(int i = 0; i < world->ringCount; i++)
        destroyRing(world->rings[i]);
    FFREE(world->rings);
    FFREE(world);
}


void drawPlanet(Context *context, Planet *planet, World *world)
{
    float sunx, suny, sunz;
    getCoords(&world->sun, world->sunPosition, &sunx, &suny, &sunz);
    int colorSteps = context->options[OPTION_PLANET_COLOR_STEPS];
    int hasShadow = getOption(context, OPTION_SHADOW);
    for(int i = -planet->size; i <= planet->size; i++)
    {
        for(int j = -planet->size; j <= planet->size; j++)
        {
            int x = context->width / 2 + i;
            int y = context->height / 2 + j;
            if(x < 0 || x > context->width || y < 0 || y > context->height)
                continue;
            float fi = (float)i + (i > 0 ? 0.5 : 0) + (i < 0 ? -0.5 : 0);
            float fj = (float)j + (j > 0 ? 0.5 : 0) + (j < 0 ? -0.5 : 0);
            float xn = fi / (float)(planet->size);
            float yn = fj / (float)(planet->size);
            float z;
            if(getSphereCoords(xn, yn, &z) == 0)
            {
                float dotSun = xn * sunx + yn * suny + z * sunz;
                rotateCoords(&planet->axis, &xn, &yn, &z, planet->rotation);
                float noise = perlinOctave(context, xn / planet->perlinScale, yn / planet->perlinScale, z / planet->perlinScale, 4, 0.4);
                Uint32 color;
                int step = (int)floorf(noise * colorSteps);
                if(hasShadow && dotSun < 0)
                    color = context->colors[context->colorids[COLOR_PLANET_SHADOW] + planet->colorNo * colorSteps + step];
                else
                    color = context->colors[context->colorids[COLOR_PLANET] + planet->colorNo * colorSteps + step];
                setPixel(context, x, y, 0, color);
            }
        }
    }
}

void drawSatellite(Context *context, Satellite *satellite, World *world)
{
    float sx, sy, sz;
    getCoords(&(satellite->orbit), satellite->position, &sx, &sy, &sz);
    float sunx, suny, sunz;
    getCoords(&world->sun, world->sunPosition, &sunx, &suny, &sunz);
    Uint32 color = context->colors[context->colorids[COLOR_SATELLITE] + satellite->colorNo];
    Uint32 colorShadow = context->colors[context->colorids[COLOR_SATELLITE_SHADOW] + satellite->colorNo];
    int hasShadow = getOption(context, OPTION_SHADOW);
    for(int i = -satellite->size; i <= satellite->size; i++)
    {
        for(int j = -satellite->size; j <= satellite->size; j++)
        {
            int x = context->width / 2 + i + (int)floorf(sx);
            int y = context->height / 2 + j + (int)floorf(sy);
            if(x < 0 || x > context->width || y < 0 || y > context->height)
                continue;
            float fi = (float)i + (i > 0 ? 0.5 : 0) + (i < 0 ? -0.5 : 0);
            float fj = (float)j + (j > 0 ? 0.5 : 0) + (j < 0 ? -0.5 : 0);
            float xn = fi / (float)(satellite->size);
            float yn = fj / (float)(satellite->size);
            float z;
            if(getSphereCoords(xn, yn, &z) == 0)
            {
                float dotSun = xn * sunx + yn * suny + z * sunz;
                if(hasShadow && dotSun < 0)
                    setPixel(context, x, y, (int)ceilf(sz), colorShadow);
                else
                    setPixel(context, x, y, (int)ceilf(sz), color);
            }
        }
    }
}

void drawRing(Context *context, Ring *ring)
{
    float px, py, z;
    Trajectory t = {.pitch = ring->orbit.pitch, .yaw = ring->orbit.yaw, .radius = ring->orbit.radius, .speed = ring->orbit.speed};
    int colorSteps = context->options[OPTION_RING_COLOR_STEPS];
    for(int p = 0; p < ring->count; p++)
    {
        t.radius = ring->distances[p];
        getCoords(&t, ring->positions[p], &px, &py, &z);
        for(int i = -ring->sizes[p]; i <= ring->sizes[p]; i++)
        {
            for(int j = -ring->sizes[p]; j <= ring->sizes[p]; j++)
            {
                int x = context->width / 2 + i + (int)floorf(px);
                int y = context->height / 2 + j + (int)floorf(py);
                if(x < 0 || x > context->width || y < 0 || y > context->height)
                    continue;
                if(abs(i) + abs(j) < ring->sizes[p])
                    setPixel(context, x, y, (int)ceilf(z), context->colors[context->colorids[COLOR_RING] + ring->colorNo * colorSteps + ring->colors[p]]);
            }
        }
    }
}

void drawStars(Context *context, Stars *stars)
{
    Uint32 background = context->colors[context->colorids[COLOR_SKY]];
    for(int i = 0; i < context->width * context->height; i++)
        context->pixels[i] = background;
    float maxRadius = (float)getOption(context, OPTION_RADIUS_MAX);
    int shadow = getOption(context, OPTION_STAR_SHADOW);
    Uint32 color;
    Uint32 twinkleColor = context->colors[context->colorids[COLOR_STAR_TWINKLE]];
    for(int i = 0; i < stars->count; i++)
    {
        float distance = sqrtf(stars->x[i] * stars->x[i] + stars->y[i] * stars->y[i]) / maxRadius;
        if(distance > 1)
            distance = 1;
        if(shadow)
            color = interpolate(context->colors[context->colorids[COLOR_STAR]], context->colors[context->colorids[COLOR_SKY]], distance);
        else
            color = context->colors[context->colorids[COLOR_STAR]];
        setPixel(context, stars->x[i] + (context->width / 2), stars->y[i] + (context->height / 2), 1000, color);
        for(int j = 1; j <= stars->sizes[i]; j++)
        {
            float alpha = (float)(stars->sizes[i] + 1 - j) / (float)(stars->sizes[i] + 1) * (shadow ? distance : 1.0);
            color = interpolate(context->colors[context->colorids[COLOR_STAR]], context->colors[context->colorids[COLOR_SKY]], alpha);
            setPixel(context, stars->x[i] + (context->width / 2) - j, stars->y[i] + (context->height / 2), 1000 + j, color);
            setPixel(context, stars->x[i] + (context->width / 2) + j, stars->y[i] + (context->height / 2), 1000 + j, color);
            setPixel(context, stars->x[i] + (context->width / 2), stars->y[i] + (context->height / 2) - j, 1000 + j, color);
            setPixel(context, stars->x[i] + (context->width / 2), stars->y[i] + (context->height / 2) + j, 1000 + j, color);
        }
        if(stars->twinkling[i] > 0)
        {
            setPixel(context, stars->x[i] + (context->width / 2) - stars->twinkling[i], stars->y[i] + (context->height / 2), 999, twinkleColor);
            setPixel(context, stars->x[i] + (context->width / 2) + stars->twinkling[i], stars->y[i] + (context->height / 2), 999, twinkleColor);
            setPixel(context, stars->x[i] + (context->width / 2), stars->y[i] + (context->height / 2) - stars->twinkling[i], 999, twinkleColor);
            setPixel(context, stars->x[i] + (context->width / 2), stars->y[i] + (context->height / 2) + stars->twinkling[i], 999, twinkleColor);
        }
    }
}

void drawWorld(Context *context, World *world)
{
    drawStars(context, world->stars);
    drawPlanet(context, world->planet, world);
    
    for(int i = 0; i < world->satelliteCount; i++)
        drawSatellite(context, world->satellites[i], world);

    for(int i = 0; i < world->ringCount; i++)
        drawRing(context, world->rings[i]);
}


Uint32 interpolate(Uint32 color1, Uint32 color2, float alpha)
{
    Uint32 r1 = (color1 & 0xFF0000) >> 16;
    Uint32 g1 = (color1 & 0x00FF00) >> 8;
    Uint32 b1 = color1 & 0x0000FF;
    Uint32 r2 = (color2 & 0xFF0000) >> 16;
    Uint32 g2 = (color2 & 0x00FF00) >> 8;
    Uint32 b2 = color2 & 0x0000FF;

    Uint32 result = (((int)(r1 * alpha) + (int)(r2 * (1.0 - alpha))) << 16) |
           (((int)(g1 * alpha) + (int)(g2 * (1.0 - alpha))) << 8) |
           (((int)(b1 * alpha) + (int)(b2 * (1.0 - alpha))));
    return result;
}

void setPixel(Context *context, int x, int y, int z, int c)
{
    if(0 <= x && x < context->width && 0 <= y && y < context->height && z < context->renderZ[x + y * context->width])
    {
        context->pixels[x + y * context->width] = c;
        context->renderZ[x + y * context->width] = z;
    }
}


void tickWorld(Context *context, World *world, float timeDiff)
{
    if(!world)
        return;

    if(getOption(context, OPTION_STAR_TWINKLING))
    {
        float randomTwinkle = 1.0 / ((float)getOption(context, OPTION_STAR_TWINKLE_PERIOD) * (float)world->stars->count);
        world->stars->twinkleTime += timeDiff;
        if(world->stars->twinkleTime > STAR_TWINKLE_TIME)
        {
            world->stars->twinkleTime -= STAR_TWINKLE_TIME;
            for(int i = 0; i < world->stars->count; i++)
            {
                if(world->stars->twinkling[i] > 0)
                {
                    world->stars->twinkling[i]++;
                    if(world->stars->twinkling[i] > STAR_TWINKLE_FRAMES)
                        world->stars->twinkling[i] = 0;
                }
                else
                    world->stars->twinkling[i] = (RAND() < randomTwinkle) ? 1 : 0;
            }
        }
    }

    if(getOption(context, OPTION_SHADOW_ROTATION))
        world->sunPosition += fmodf(world->sun.speed * timeDiff, 1.0);
    if(getOption(context, OPTION_PLANET_ROTATION))
        world->planet->rotation += fmodf(world->planet->axis.speed * timeDiff, 1.0);

    for(int i = 0; i < world->satelliteCount; i++)
        world->satellites[i]->position = fmodf(world->satellites[i]->position + world->satellites[i]->orbit.speed * timeDiff, 1.0);

    for(int i = 0; i < world->ringCount; i++)
    {
        for(int j = 0; j < world->rings[i]->count; j++)
        {
            world->rings[i]->positions[j] += world->rings[i]->orbit.speed * timeDiff * (world->rings[i]->orbit.radius / world->rings[i]->distances[j]);
            world->rings[i]->positions[j] = fmodf(world->rings[i]->positions[j], 1.0);
        }
    }
}


void getCoords(Trajectory *trajectory, float position, float *x, float *y, float *z)
{
    // x = right
    // y = down
    // z = far
    if (!trajectory || !x || !y || !z)
        return;
    float radVal = position * M_PI * 2.0;
    *x = trajectory->radius * cosf(radVal);
    *y = 0;
    *z = trajectory->radius * sinf(radVal);
    radVal = trajectory->pitch * M_PI * 2.0;
    float ta = *x * cosf(radVal) - *y * sinf(radVal);
    float tb = *x * sinf(radVal) + *y * cosf(radVal);
    *x = ta;
    *y = -tb;
    radVal = trajectory->yaw * M_PI * 2.0;
    ta = *x * cosf(radVal) - *z * sinf(radVal);
    tb = *x * sinf(radVal) + *z * cosf(radVal);
    *x = ta;
    *z = tb;
}

int getSphereCoords(float x, float y, float *z)
{
    if(!z)
        return 1;
    if(x * x + y * y >= 1)
        return 1;
    *z = sqrtf(1 - x * x - y * y);
    return 0;
}

void rotateCoords(Trajectory *axis, float *x, float *y, float *z, float angle)
{
    if(!axis ||!x ||!y || !z)
        return;
    float qx = 0, qy = 0, qz = 0;
    float costheta = cos(angle * M_PI * 2);
    float sintheta = sin(angle * M_PI * 2);

    qx += (costheta + (1 - costheta) * axis->x * axis->x) * (*x);
    qx += ((1 - costheta) * axis->x * axis->y - axis->z * sintheta) * (*y);
    qx += ((1 - costheta) * axis->x * axis->z + axis->y * sintheta) * (*z);

    qy += ((1 - costheta) * axis->x * axis->y + axis->z * sintheta) * (*x);
    qy += (costheta + (1 - costheta) * axis->y * axis->y) * (*y);
    qy += ((1 - costheta) * axis->y * axis->z - axis->x * sintheta) * (*z);

    qz += ((1 - costheta) * axis->x * axis->z - axis->y * sintheta) * (*x);
    qz += ((1 - costheta) * axis->y * axis->z + axis->x * sintheta) * (*y);
    qz += (costheta + (1 - costheta) * axis->z * axis->z) * (*z);

    *x = qx;
    *y = qy;
    *z = qz;
}


void perlinInit(Context *context)
{
    if(!context)
        return;
    if(context->perlinSeed != NULL)
        FFREE(context->perlinSeed);
    MMALLOC(context->perlinSeed, int, 512);
    for(int i = 0; i < 256; i++)
        context->perlinSeed[i] = i;
    for(int i = 0; i < 256; i++)
    {
        int j = rand() & 0xFF;
        int temp = context->perlinSeed[i];
        context->perlinSeed[i] = context->perlinSeed[j];
        context->perlinSeed[j] = temp;
    }
    for(int i = 0; i < 256; i++)
        context->perlinSeed[i + 256] = context->perlinSeed[i];
}

float perlinFade(float t)
{
    return t * t * t * (t * (t * 6.0 - 15.0) + 10.0);
}

int perlinInc(int num) {
    return (num + 1) % 256;
}

float perlinGrad(int hash, float x, float y, float z) {
    int h = hash & 15;
    float u = h < 8 ? x : y;
    float v = h < 4 ? y : h == 12 || h == 14 ? x : z;
    return ((h&1) == 0 ? u : -u)+((h&2) == 0 ? v : -v);
}

float perlinLerp(float a, float b, float x) {
    return a + x * (b - a);
}

float perlin(Context *context, float x, float y, float z)
{
    x = fmodf(x * 256.0, 256.0);
    x = x < 0 ? x + 256.0 : x;
    y = fmodf(y * 256.0, 256.0);
    y = y < 0 ? y + 256.0 : y;
    z = fmodf(z * 256.0, 256.0);
    z = z < 0 ? z + 256.0 : z;
    int xi = (int)(x);
    int yi = (int)(y);
    int zi = (int)(z);
    float xf = x - xi;
    float yf = y - yi;
    float zf = z - zi;
    float u = perlinFade(xf);
    float v = perlinFade(yf);
    float w = perlinFade(zf);

    int aaa, aba, aab, abb, baa, bba, bab, bbb;
    int *p = context->perlinSeed;
    aaa = p[p[p[          xi ]+          yi ]+          zi ];
    aba = p[p[p[          xi ]+perlinInc(yi)]+          zi ];
    aab = p[p[p[          xi ]+          yi ]+perlinInc(zi)];
    abb = p[p[p[          xi ]+perlinInc(yi)]+perlinInc(zi)];
    baa = p[p[p[perlinInc(xi)]+          yi ]+          zi ];
    bba = p[p[p[perlinInc(xi)]+perlinInc(yi)]+          zi ];
    bab = p[p[p[perlinInc(xi)]+          yi ]+perlinInc(zi)];
    bbb = p[p[p[perlinInc(xi)]+perlinInc(yi)]+perlinInc(zi)];

    float x1, x2, y1, y2;
    x1 = perlinLerp(perlinGrad(aaa, xf, yf, zf), perlinGrad(baa, xf-1, yf  , zf), u);
    x2 = perlinLerp(perlinGrad(aba, xf, yf-1, zf), perlinGrad(bba, xf-1, yf-1, zf), u);
    y1 = perlinLerp(x1, x2, v);
    x1 = perlinLerp(perlinGrad(aab, xf, yf, zf-1),perlinGrad(bab, xf-1, yf, zf-1), u);
    x2 = perlinLerp(perlinGrad(abb, xf, yf-1, zf-1),perlinGrad (bbb, xf-1, yf-1, zf-1), u);
    y2 = perlinLerp (x1, x2, v);

    return (perlinLerp(y1, y2, w) + 1) / 2;
}

float perlinOctave(Context *context, float x, float y, float z, int octaves, float persistence)
{
    float total = 0;
    float frequency = 1;
    float amplitude = 1;
    float maxValue = 0;
    for(int i = 0; i < octaves; i++) {
        total += perlin(context, x * frequency, y * frequency, z * frequency) * amplitude;

        maxValue += amplitude;

        amplitude *= persistence;
        frequency *= 2;
    }

    return total / maxValue;
}

