#include <SDL2/SDL.h>

#include <stdio.h>
#include <stdlib.h>
#include "worlds.h"
#include "defs.h"
#include <stdbool.h>
#include <time.h>
#include <limits.h>

bool toReload = false;

#if defined(__unix__) || defined(__linux__)
#include<signal.h>
#include<unistd.h>
void reloadHandler(int signum)
{
    toReload = true;
}
#endif


void reload(Context *context, World **world)
{
    if(!context || !world || !(*world))
        return;
    destroyWorld(*world);
    readOptions(context);
    perlinInit(context);
    resizeContext(context);
    *world = makeWorld(context);
}


int main(int argc, char** argv)
{
    srand(time(NULL));
#ifdef __unix__
    signal(SIGUSR1, reloadHandler);
#endif

    if(SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        fprintf(stderr, "Failed SDL initialization (%s)\n",SDL_GetError());
        return EXIT_FAILURE;
    }
    
    SDL_Window* window = NULL;
    if(argc < 2)
    {
        window = SDL_CreateWindow("Screen Worlds", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_RESIZABLE);
    }
    else
    {
        int winid = strtol(argv[1], NULL, 16);
        window = SDL_CreateWindowFrom((void*)winid);
    }
    
    if(window)
    {
        SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
        Context *context = makeContext(renderer);

        unsigned int framerate = 1000 / getOption(context, OPTION_FRAME_RATE);
        int scale = getOption(context, OPTION_RENDER_SCALE);
        int windowWidth, windowHeight;
        SDL_GetRendererOutputSize(context->renderer, &windowWidth, &windowHeight);
        SDL_Rect dstRect = {.x = 0, .y = 0, .w = windowWidth + windowWidth % scale, .h = windowHeight + windowHeight % scale};

        World *world = makeWorld(context);

        SDL_Event event;
        bool done = false;
        unsigned int lastFrame = SDL_GetTicks();
        while(!done)
        {
            while(SDL_PollEvent(&event))
            {
                switch(event.type)
                {
                    case SDL_QUIT:
                        done = true;
                        break;
                    case SDL_KEYDOWN:
                        switch(event.key.keysym.sym)
                        {
                            case SDLK_ESCAPE:
                                done = true;
                                break;
                            case SDLK_r:
                                reload(context, &world);
                                framerate = 1000 / getOption(context, OPTION_FRAME_RATE);
                                scale = getOption(context, OPTION_RENDER_SCALE);
                                SDL_GetRendererOutputSize(context->renderer, &windowWidth, &windowHeight);
                                dstRect.w = windowWidth + windowWidth % scale;
                                dstRect.h = windowHeight + windowHeight % scale;
                                break;
                        }
                        break;
                    case SDL_WINDOWEVENT:
                        switch(event.window.event)
                        {
                            case SDL_WINDOWEVENT_RESIZED:
                                resizeContext(context);
                                SDL_GetRendererOutputSize(context->renderer, &windowWidth, &windowHeight);
                                dstRect.w = windowWidth + windowWidth % scale;
                                dstRect.h = windowHeight + windowHeight % scale;
                                break;
                        }
                }
            }
            if(toReload)
            {
                reload(context, &world);
                framerate = 1000 / getOption(context, OPTION_FRAME_RATE);
                scale = getOption(context, OPTION_RENDER_SCALE);
                SDL_GetRendererOutputSize(context->renderer, &windowWidth, &windowHeight);
                dstRect.w = windowWidth + windowWidth % scale;
                dstRect.h = windowHeight + windowHeight % scale;
                toReload = false;
            }

            unsigned int currentFrame = SDL_GetTicks();
            if(currentFrame - lastFrame < framerate)
            {
                SDL_Delay(1);
                continue;
            }
            tickWorld(context, world, (float)(currentFrame - lastFrame) / 1000.0);
            lastFrame = currentFrame;

            void *pixels = NULL;
            int pitch;
            SDL_LockTexture(context->renderTexture, NULL, &pixels, &pitch);
            context->pixels = pixels;
            for(int z = 0; z < context->width * context->height; z++)
                context->renderZ[z] = INT_MAX;

            drawWorld(context, world);

            context->pixels = NULL;
            SDL_UnlockTexture(context->renderTexture);
            SDL_RenderCopy(renderer, context->renderTexture, NULL, &dstRect);
            SDL_RenderPresent(renderer);
        }

        destroyContext(context);
        destroyWorld(world);
    }
    else
    {
        fprintf(stderr,"Failed window creation (%s)\n",SDL_GetError());
        SDL_Quit();
        return EXIT_FAILURE;
    }

    SDL_DestroyWindow(window);
    SDL_Quit();
    return EXIT_SUCCESS;
}